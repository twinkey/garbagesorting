using DG.Tweening;
using UnityEngine;

public class BinNormal : MonoBehaviour
{
    [SerializeField] Trash.TrashType _BinType;

    [SerializeField] AudioSource _SFXGood;
    [SerializeField] AudioSource _SFXBad;

    [SerializeField] private ParticleSystem _goodParticle;
    [SerializeField] private ParticleSystem _badParticle;
    [SerializeField] private ParticleSystem _destroyParticle;

    private void OnTriggerEnter(Collider other)
    {
        var trash = other.GetComponent<Trash>();

        if (trash == null)
            return;

        //trash.GetComponent<Rigidbody>().isKinematic = true;
        if (_BinType == trash._TrashType)
        {
            GameManager.Instance.Good();
            _SFXGood.Play();
            _goodParticle.Play();
        }
        else
        {
            GameManager.Instance.Bad();
            _SFXBad.Play();
            _badParticle.Play();
        }

        _destroyParticle.transform.position = trash.transform.position;

        DOVirtual.DelayedCall(1, () => _destroyParticle.Play());

        Destroy(trash.gameObject, 1);
    }
}