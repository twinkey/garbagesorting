using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class LauncherRotator : MonoBehaviour
{
    [SerializeField] private float _maxXRotation = 10;
    [SerializeField] private float _maxYRotation = 10;
    
    private Vector3 _startingRotation;

    private void Awake()
    {
        _startingRotation = transform.eulerAngles;
    }

    private void Start()
    {
        FindObjectOfType<Launcher>().OnLaunchEvent.AddListener(DoRandomRotate);
    }

    // Invoked by UnityEvent in Launcher
    public void DoRandomRotate()
    {
        float randomX = Random.Range(-_maxXRotation, _maxXRotation);

        float randomY = Random.Range(-_maxYRotation, _maxYRotation);
        // float randomY = CoinFlip() ? -_maxYRotation : _maxYRotation;

        Vector3 randomRotation = new Vector3(randomX, randomY, 0);
        Vector3 newRotation = _startingRotation + randomRotation;
        // Quaternion randomRotationQuaternion = Quaternion.Euler(randomRotation);
        transform.DORotate(newRotation, 0.5f).SetEase(Ease.InExpo);
    }

    private bool CoinFlip()
    {
        return Random.Range(0, 2) == 0;
    }
}