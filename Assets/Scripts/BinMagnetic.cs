using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;
using DG.Tweening;

public class BinMagnetic : MonoBehaviour
{
    [SerializeField] Trash.TrashType _BinType;

    [SerializeField] AudioSource _SFXGood;
    [SerializeField] AudioSource _SFXBad;

    [SerializeField] private Transform _magneticPoint1;
    [SerializeField] private Transform _magneticPoint2;

    [SerializeField] private float _point1Dir = 0.7f;
    [SerializeField] private float _point2Dir = 0.7f;

    [SerializeField] private AnimationCurve _point1Ease;
    [SerializeField] private AnimationCurve _point2Ease;

    [SerializeField] private GameObject _binGfx;
    [SerializeField] private float _binGfxCurveScale = 1.1f;
    [SerializeField] private AnimationCurve _binGfxCurve;
    [SerializeField] private float _binGfxCurveDuration;
    [SerializeField] private ParticleSystem _portalParticle;
    [SerializeField] private ParticleSystem _goodParticle;
    [SerializeField] private ParticleSystem _badParticle;


    private void OnTriggerEnter(Collider other)
    {
        var trash = other.GetComponent<Trash>();

        if (trash == null)
            return;

        trash.GetComponent<Rigidbody>().isKinematic = true;
        _binGfx.transform.DOScale(Vector3.one * _binGfxCurveScale, _binGfxCurveDuration).SetEase(_binGfxCurve);
        _portalParticle.Play();
        Sequence s = DOTween.Sequence();
        s.Append(trash.transform.DOMove(_magneticPoint1.position, _point1Dir).SetEase(_point1Ease));
        s.Append(trash.transform.DOMove(_magneticPoint2.position, _point2Dir).SetEase(_point2Ease));
        s.OnComplete(() =>
        {
            if (_BinType == trash._TrashType)
            {
                GameManager.Instance.Good();
                _SFXGood.Play();
                _goodParticle.Play();
            }
            else
            {
                GameManager.Instance.Bad();
                _SFXBad.Play();
                _badParticle.Play();
            }

            Destroy(trash.gameObject, 0.1f); // Ten delay jest, bo bez niego leca warningi, nie wiem dlaczego.
        });

        s.Play();
    }
}