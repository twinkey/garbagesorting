using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject ObjectToSpawn;
    
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.J))
            Instantiate(ObjectToSpawn, transform.position, Quaternion.identity);
    }
}
