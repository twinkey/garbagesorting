using System;
using BNG;
using UnityEngine;

public class Bat : MonoBehaviour
{
    [SerializeField] ParticleSystem _boomParticle;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionExit(Collision collision)
    {
        Trash trash = collision.gameObject.GetComponent<Trash>();
        if (trash != null)
        {
            _boomParticle.transform.parent = null;
            _boomParticle.transform.position = trash.transform.position;
            trash.GetComponent<Rigidbody>().velocity *= 2;
            _boomParticle.Play();
            _audioSource.Play();
            
            Grabber thisGrabber = GetComponent<Grabbable>().GetPrimaryGrabber();
            InputBridge.Instance.VibrateController(0.5f, 1, 0.01f, thisGrabber.HandSide);                    
            
            // Destroy(trash.gameObject);
        }
    }
}
