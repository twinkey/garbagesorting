using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;
using DG.Tweening;

public class Launcher : MonoBehaviour
{
    [SerializeField] private float _power = 100;
    [SerializeField] private float _maxTorque = 0.5f;
    [SerializeField] private GameObject _launchPoint;
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem _particleExplosion;
    [SerializeField] private AudioSource _SFXShoot;

    [Space]
    [Header("=== Cutscene ==")]
    [SerializeField] private Transform _DestPos;
    [SerializeField] private ParticleSystem _particleTeleport;
    [SerializeField] private ParticleSystem _particleStartTeleport;
    [SerializeField] private float _CutsceneStartDelay = 1f;
    [SerializeField] private float _MoveDuradion = 1f;
    [SerializeField] private AudioSource _SFXTeleport;

    public UnityEvent OnLaunchEvent;

    private float _remainingCooldown;
    private int _currentTrashIndex = 0;

    private Vector3 _StartPos;

    [SerializeField] private List<GameObject> _TrashesToSpawn;
    private static readonly int SHOOT = Animator.StringToHash("Shoot");

    public void Show()
    {
        _particleStartTeleport.Play();
        _SFXTeleport.Play();
        _particleTeleport.Play();
        StartCoroutine(ShowCutscene());
    }

    private IEnumerator ShowCutscene()
    {
        transform.DOMove(_StartPos, _MoveDuradion);
        yield return new WaitForSeconds(_CutsceneStartDelay);
        _particleStartTeleport.Play();
        _particleTeleport.Stop();
    }

    public void Hide()
    {
        _SFXTeleport.Play();
        _particleStartTeleport.Play();
        _particleTeleport.Play();
        StartCoroutine(HideCutscene());
    }

    private IEnumerator HideCutscene()
    {
        transform.DOMove(_DestPos.position, _MoveDuradion);
        yield return new WaitForSeconds(_CutsceneStartDelay);
        _particleStartTeleport.Play();
        _particleTeleport.Stop();
    }


    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        _StartPos = transform.position;
        transform.position = _DestPos.position;
        _TrashesToSpawn.Shuffle();
    }

    public void StartShootAnimation()
    {
        _animator.SetTrigger(SHOOT);
    }

    public void Shoot()
    {
        Trash trash = Instantiate(_TrashesToSpawn[_currentTrashIndex], _launchPoint.transform.position, Quaternion.identity).GetComponent<Trash>();
        Rigidbody rigid = trash.GetComponent<Rigidbody>();
        rigid.AddForce(_launchPoint.transform.forward * _power, ForceMode.Impulse);
        
        float x = Random.Range(-_maxTorque, _maxTorque);
        float y = Random.Range(-_maxTorque, _maxTorque);
        float z = Random.Range(-_maxTorque, _maxTorque);
        
        rigid.AddTorque(x, y, z);
        OnLaunchEvent.Invoke();

        _SFXShoot.Play();
        _particleExplosion.Play();
        
        IncrementCurrentTrashIndex();
    }

    private void IncrementCurrentTrashIndex()
    {
        _currentTrashIndex++;
        if (_currentTrashIndex >= _TrashesToSpawn.Count)
        {
            _currentTrashIndex = 0;
            _TrashesToSpawn.Shuffle();
        }
    }
}