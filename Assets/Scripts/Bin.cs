﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;

public class Bin : MonoBehaviour
{
    [SerializeField] Trash.TrashType _BinType;

    [SerializeField] AudioSource _SFXGood;
    [SerializeField] AudioSource _SFXBad;

    private void OnTriggerEnter(Collider other)
    {
        var trash = other.GetComponent<Trash>();

        if (trash)
        {
            if (_BinType == trash._TrashType)
            {
                GameManager.Instance.Good();
                _SFXGood.Play();
            }
            else
            {
                GameManager.Instance.Bad();
                _SFXBad.Play();
            }

            Destroy(trash.gameObject);
        }
    }
}