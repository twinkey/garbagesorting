using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    private List<Launcher> _launchers;

    [SerializeField, Range(10, 180)] private float _GameTime = 120f;
    [SerializeField] private float _TimeToSpeedUpGame = 30f;
    [SerializeField] private float _Speeduper = .5f;
    [SerializeField] private float _IncreaseTimeOnGood = 2f;
    [SerializeField] private float _DecreaseTimeOnBad = 1f;

    [Space]
    [Header("=== Cutscene ==")]
    [SerializeField] private float _CutsceneStart = 1f;

    [Space]
    [SerializeField] private Vector2 _ShootRandomizer = new Vector2(1f, 3f);

    [Space]
    [Header("=== SFX ==")]
    [SerializeField] private AudioSource _GameMusic;
    [SerializeField] private AudioSource _IdleMusic;
    [SerializeField] private AudioSource _EndGameSFX;
    [SerializeField] private float _fadeMusicTime = 0.5f;
    [SerializeField] private float _MainMusicVolume = 0.5f;
    [SerializeField] private float _IdleMusicVolume = 0.15f;

    [Space]
    [Header("=== UI ==")]
    [SerializeField] GameObject _TimeUI;
    [SerializeField] Image _TimeBar;
    [SerializeField] GameObject _ResultUI;
    [SerializeField] TMPro.TextMeshProUGUI _ResultText;

    private Vector3 _TimeBarStartScale;
    private Vector3 _TimeBarCurrentScale;

    private float _baseTimer;
    private float _gameTimer;
    private float _spawnerTimer = 0;
    private float _spawnerRandom;

    private int _pointsCounter;

    private Coroutine _coroutine;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this);

        _launchers = FindObjectsOfType<Launcher>().ToList();
    }

    void Start()
    {
        _TimeBar.fillAmount = 0;

        _IdleMusic.Play();
        _IdleMusic.DOFade(_IdleMusicVolume, _fadeMusicTime);

        _TimeUI.SetActive(false);
        _ResultUI.SetActive(false);
    }

    public void StartGame()
    {
        StartCoroutine(StartCutscene());
    }

    private IEnumerator StartCutscene()
    {
        _launchers.ForEach(l => l.Show());

        yield return new WaitForSeconds(_CutsceneStart);

        _GameMusic.Play();
        _GameMusic.DOFade(_MainMusicVolume, _fadeMusicTime);

        _IdleMusic.DOFade(0, _fadeMusicTime);

        ResetGame();

        _TimeUI.SetActive(true);
        _ResultUI.SetActive(false);

        _coroutine = StartCoroutine(GameRoutine());
    }

    private IEnumerator GameRoutine()
    {
        while (_gameTimer > 0)
        {
            _gameTimer -= Time.deltaTime;
            _baseTimer += Time.deltaTime;

            TrySpawnTrash();
            UpdateUI();
            yield return null;
        }

        EndGame();
    }

    private void UpdateUI()
    {
        float time = _gameTimer / _GameTime;

        float newValue = Mathf.Lerp(0, 1, time);

        _TimeBar.fillAmount = newValue;
    }

    private void TrySpawnTrash()
    {
        _spawnerTimer += Time.deltaTime;

        if (_spawnerRandom < _spawnerTimer)
        {
            float t = _baseTimer / _TimeToSpeedUpGame;
            float speeduper = Mathf.Lerp(1, _Speeduper, t);

            _spawnerTimer = 0;
            _spawnerRandom = Random.Range(_ShootRandomizer.x, _ShootRandomizer.y);
            _spawnerRandom *= speeduper;

            SpawnTrash();
        }
    }

    private void SpawnTrash()
    {
        _launchers.ForEach(l => l.StartShootAnimation());
    }

    private void EndGame()
    {
        _launchers.ForEach(l => l.Hide());

        _TimeUI.SetActive(false);
        _ResultUI.SetActive(true);

        _ResultText.text = "Wynik: " + _pointsCounter;

        _GameMusic.DOFade(0, _fadeMusicTime);
        _EndGameSFX.Play();

        _IdleMusic.Play();
        _IdleMusic.DOFade(_IdleMusicVolume, _fadeMusicTime);
    }

    public void ResetGame()
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _TimeBar.fillAmount = 0;
        _pointsCounter = 0;
        _spawnerRandom = 0;

        _baseTimer = 0;
        _gameTimer = _GameTime;
    }

    public void Good()
    {
        _gameTimer += _IncreaseTimeOnGood;
        _pointsCounter++;
    }

    public void Bad()
    {
        _gameTimer -= _DecreaseTimeOnBad;
    }
}