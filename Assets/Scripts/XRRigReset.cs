using BNG;
using UnityEngine;

public class XRRigReset : MonoBehaviour
{
    private InputBridge _inputBridge;
    private Vector3 _startingPosition;
    //private Quaternion _startingRotation;
    private float _startingRotationY;
    private Transform _transform;

    private void Start()
    {
        _transform = transform;
        _inputBridge = InputBridge.Instance;

        _startingPosition = _transform.position;
        _startingRotationY = _transform.rotation.y;
    }

    private void LateUpdate()
    {
        if (_inputBridge.RightThumbstickUp || _inputBridge.LeftThumbstickUp || Input.GetKeyUp(KeyCode.R))
        {
            ResetXrRig();
        }
    }

    private void ResetXrRig()
    {
        // _transform.position = _startingPosition;
        transform.position = _startingPosition;
        
        _transform.localEulerAngles = new Vector3(_transform.localEulerAngles.x, _startingRotationY, _transform.localEulerAngles.z);
    }
}